package ab20081424.foundation.bank.AB20081424_bank.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import ab20081424.foundation.bank.AB20081424_bank.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ab20081424.foundation.bank.AB20081424_bank.dao.PersonDao;
import ab20081424.foundation.bank.AB20081424_bank.model.Person;


@Service
public class PersonServiceImpl implements PersonService {

    private final PersonDao personDao ;

    @Autowired
    public PersonServiceImpl(@Qualifier("fakeDao") PersonDao personDao){
        this.personDao = personDao;
    }

    public int addPerson(Person person) {
        return personDao.insertPerson(person);
    }

    public List<Person> getAllPeople() {
        return personDao.selectAllPeople();
    }
    public Optional<Person> getPersonById(UUID id) {
        return personDao.selectPersonById(id);

    }

    public int deletePerson(UUID id){
        return personDao.deletePersonById(id);
    }

    public int updatePerson(UUID id, Person newPerson){
        return personDao.updatePersonById(id, newPerson);
    }

    @Override
    public String transferFunds(UUID from, UUID to, double amount) {
        return personDao.transferFunds(from, to, amount);
    }

}