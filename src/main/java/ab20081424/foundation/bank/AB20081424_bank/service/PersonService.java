package ab20081424.foundation.bank.AB20081424_bank.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import ab20081424.foundation.bank.AB20081424_bank.model.Person;


public interface PersonService {


    public int addPerson(Person person);

    public List<Person> getAllPeople() ;
    public Optional<Person> getPersonById(UUID id);

    public int deletePerson(UUID id);

    public int updatePerson(UUID id, Person newPerson);

    String transferFunds(UUID from, UUID to, double amount);

}