package ab20081424.foundation.bank.AB20081424_bank.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Repository;

import ab20081424.foundation.bank.AB20081424_bank.model.Person;

@Repository("fakeDao")
public class PersonDaoImpl implements PersonDao {

    private static List<Person> DB = new ArrayList<>(); //This is where we store instance data which gets purged once the app stops

    @Override
    public int insertPerson(UUID id, Person person) {
        DB.add(new Person(id, person.getName(), person.getPhno(), person.getAddress(), person.getAccount_type(), person.getAccount_balance()));
        return 1;
    }

    @Override
    public List<Person> selectAllPeople() {
        return DB;
    }

    @Override
    public Optional<Person> selectPersonById(UUID id) {
        // TODO Auto-generated method stub
        return DB.stream()
                .filter(person -> person.getID().equals(id))
                .findFirst();
    }

    @Override
    public int deletePersonById(UUID id) {
        // TODO Auto-generated method stub
        Optional<Person> personMaybe=selectPersonById(id);
        if(personMaybe.isEmpty()) {
            return 0;
        }
        DB.remove(personMaybe.get());
        return 1;
    }

    @Override
    public int updatePersonById(UUID id, Person update) {
        // TODO Auto-generated method stub
        return selectPersonById(id)
                .map(person -> {
                    int indexOfPersonToUpdate = DB.indexOf(person);
                    if(indexOfPersonToUpdate >= 0) {
                        DB.set(indexOfPersonToUpdate, new Person(id, update.getName(), update.getPhno(), update.getAddress(), update.getAccount_type(), update.getAccount_balance()));
                        return 1;
                    }
                    return 0;
                })
                .orElse(0);
        
    }

    public String transferFunds(UUID from, UUID to, double amount){
        Person fromPerson=selectPersonById(from).get();
        Person toPerson=selectPersonById(to).get();
        if (null ==  fromPerson || null == toPerson){
            return "ID MISMATCH";
        }
        if(fromPerson.getAccount_balance() < amount){
            return "INSUFFICIENT FUNDS";
        }
        else{
            fromPerson.setAccount_balance(fromPerson.getAccount_balance() - amount);
            toPerson.setAccount_balance(toPerson.getAccount_balance() + amount);
            return "SUCCESS";
        }
    }


}