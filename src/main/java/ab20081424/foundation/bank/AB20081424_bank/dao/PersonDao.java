package ab20081424.foundation.bank.AB20081424_bank.dao; //instance

import ab20081424.foundation.bank.AB20081424_bank.model.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PersonDao {
    int insertPerson(UUID id, Person person);

    default int insertPerson(Person person){
        UUID id = UUID.randomUUID();
        return insertPerson(id, person);
    }

    List<Person> selectAllPeople();
    
    Optional<Person> selectPersonById(UUID id);

    int deletePersonById(UUID id);

    int updatePersonById(UUID id, Person person);

    String transferFunds(UUID from, UUID to, double amount);

}