package ab20081424.foundation.bank.AB20081424_bank.model; //model

import java.util.UUID;


import com.fasterxml.jackson.annotation.JsonProperty;

   

public class Person{
    private  UUID id;
    private  String name;
    private  String phno;
    private  String address;
    private  String account_type;
    private  double account_balance;

    public Person(@JsonProperty ("id") UUID id,
                  @JsonProperty ("name") String name,
                  @JsonProperty ("phno") String phno,
                  @JsonProperty ("address") String address,
                  @JsonProperty ("account_type") String account_type,
                  @JsonProperty ("account_balance") double account_balance)
    {
        this.id = id;
        this.name = name;
        this.phno = phno;
        this.address = address;
        this.account_type = account_type;
        this.account_balance = account_balance;

    }
    public UUID getID(){
        return id;
    }
    public String getName(){
        return name;
    }
    public String getPhno(){ return phno;}
    public String getAddress(){return address;}
    public String getAccount_type(){return account_type;}
    public double getAccount_balance() {return account_balance; }


    public void setName(String name) {
        this.name = name;
    }
    public void setPhno(String phno) {
        this.phno = phno;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }
    public void setAccount_balance(double account_balance) {
        this.account_balance = account_balance;
    }
}