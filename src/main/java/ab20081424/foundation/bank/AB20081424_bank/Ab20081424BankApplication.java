package ab20081424.foundation.bank.AB20081424_bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ab20081424BankApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ab20081424BankApplication.class, args);
	}

}
