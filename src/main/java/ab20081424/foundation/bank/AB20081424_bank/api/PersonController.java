package ab20081424.foundation.bank.AB20081424_bank.api;

import java.util.List;
import java.util.UUID;

import ab20081424.foundation.bank.AB20081424_bank.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ab20081424.foundation.bank.AB20081424_bank.model.Person;


@RequestMapping("api/v1/person")

@RestController
public class PersonController {

    @Autowired
    private PersonService personService;

    @PostMapping     // public String addAccount(Account acc);
    public void addPerson(@RequestBody Person person){
        personService.addPerson(person);
    }

    @GetMapping      //public List<Account> getAllAccounts();
    public List<Person> getAllPeople() {
        return personService.getAllPeople();
    }
    @GetMapping(path="{id}")
    public Person getPersonById(@PathVariable("id") UUID id){
        return personService.getPersonById(id)
                            .orElse(null);
    }

    @RequestMapping(value = "/{id}" , method = {RequestMethod.DELETE})
    public @ResponseBody void deletePersonById(@PathVariable("id") UUID id){
        personService.deletePerson(id);
    }

    @PutMapping(path = "{id}")      //updating  a person's details
    public void updatePerson(@PathVariable("id") UUID id, @RequestBody Person personToUpdate){
        personService.updatePerson(id, personToUpdate);
    }
    @PutMapping(value = "/{from}/{to}/{amount}")
    public String transferFunds(@PathVariable("from") UUID from,
                              @PathVariable("to") UUID to,
                              @PathVariable("amount") double amount){
        return personService.transferFunds(from, to, amount);
    }
}